<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaction_details extends Model
{
    use HasFactory;

     /**
     * fillable
     * 
     * @var array
     */

    protected $fillable = [

    'qty', 'price'

    ];

    /** 
      * Transaction 
      *@return void
      */

      public function Transaction()
      {
        return $this->belongsTo(transcation::class);
      }

      /** 
      * Products
      *@return void
      */

      public function Products()
      {
        return $this->belongsTo(products::class);
      }

}
