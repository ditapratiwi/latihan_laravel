<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class carts extends Model
{
    use HasFactory;

     /**
     * fillable
     * 
     * @var array
     */

    protected $fillable = [

    'cashier_id', 'product_id', 'table_products',  'qty'
    ];

    /** 
      * Products 
      *@return void
      */

      public function Products()
      {
        return $this->belongsTo(products::class);
      }        
}
