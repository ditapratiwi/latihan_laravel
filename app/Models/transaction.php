<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\Casts\Attribute;

class transaction extends Model
{
    use HasFactory;

     /**
     * fillable
     * 
     * @var array
     */

    protected $fillable = [
    'invoice', 'cash', 'change', 'discount', 'grand_total', 'table_total'
    ];
    
    /** 
      * Details
      *@return void
      */

      public function Details()
      {
        return $this->hasMany(transaction_details::class);
      }

      /** 
      * customer 
      *@return void
      */

      public function customer()
      {
        return $this->belongsTo(Customer::class);
      }

      /** 
      * user
      *@return void
      */

      public function user()
      {
        return $this->belongsTo(User::class, 'cashier_id');
      }

      /** 
      * Profits 
      *@return void
      */

      public function Profits()
      {
        return $this->belongsTo(profits::class);
      }
      
      /**
     * createdAt
     * @return attribute
     */
    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon:: parse($value)->format('d-M-Y H:i:s'),
        );
        
    }
}
